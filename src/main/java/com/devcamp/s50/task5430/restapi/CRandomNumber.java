package com.devcamp.s50.task5430.restapi;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CRandomNumber {
     @CrossOrigin
     @GetMapping("/devcamp-welcome/random-double")
     public String randomDoubleNumber(){
          double randomDoubleNumber = 0;
          for(int i = 1; i<100; i++){
               randomDoubleNumber = Math.random() * 100;
          }
          return "Random value in double from 1 to 100: " + randomDoubleNumber;
     }

     @GetMapping("/devcamp-welcome/random-int")
     public String randomNumberInt(){
          int randomNumberInt = 1 +(int)(Math.random() * ((10-1)+1));
          return "Random value in int from 1 to 10: " + randomNumberInt;
     }
}
