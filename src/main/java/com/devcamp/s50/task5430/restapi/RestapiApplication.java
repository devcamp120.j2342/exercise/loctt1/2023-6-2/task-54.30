package com.devcamp.s50.task5430.restapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestapiApplication.class, args);

		//khai báo đối tượng class CRandomNumber
		CRandomNumber randomNumber = new CRandomNumber();
		//in ra số ngẫu nhiên từ 1 đến 100
		System.out.println(randomNumber.randomDoubleNumber());
		//In ra số ngẫu nhiên từ 1 đến 10
		System.out.println(randomNumber.randomNumberInt());
	}

}
